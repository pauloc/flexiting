module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Flexiting',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'A CSS Flex playground' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },

  /**
   * Importamos CSS externos
   */
  css: [
    'milligram/dist/milligram.min.css',
    '~/assets/css/main.css'
  ],

  /**
   * Plugin Google Analytics
   */
  plugins: [
    { src: '~plugins/ga.js', ssr: true }
  ],

  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
