import Vue from 'vue'
var bus = new Vue({
  name: 'bus',
  created() {
    this.$on('selectedItem', (index, obj) => {
      // console.log('bus index: ', index)
      if (this.selected === index) {
        // this.$emit('unselectAll', index)
      }
      this.unselectAll()
      this.selected = index
      this.currentObj = obj
      this.$emit('selected', { index, obj })
    })
  },
  data() {
    return {
      selected: 0,
      currentObj: {}
    }
  },
  methods: {
    unselectAll() {
      console.log('unselectAll.')
      this.selected = 0
      this.currentObj = null
      this.$emit('unselectAll')
    }
  }
})

export default bus
